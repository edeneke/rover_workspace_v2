#include <ros/ros.h>

// ROS libraries
#include <angles/angles.h>
#include <random_numbers/random_numbers.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>

// ROS messages
#include <std_msgs/Float32.h>
#include <std_msgs/Int16.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/String.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/Range.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <apriltags_ros/AprilTagDetectionArray.h>

// Include Controllers
#include "PickUpController.h"
#include "DropOffController.h"
#include "SearchController.h"

// To handle shutdown signals so the node quits
// properly in response to "rosnode kill"
#include <ros/ros.h>
#include <signal.h>
#include <fstream>

using namespace std;


class personalBest{
      public:
        double x;
        double y;
        double theta;
        double cost;
};

class globalBest{
      public:
        double x;
        double y;
        double theta;
        double cost;
        std_msgs::Float32 costMsg;
        std_msgs::Float32 X_Msg;
        std_msgs::Float32 Y_Msg;
};

class cost{
      public:
        double rover;     // Used to calculet global Best and personal best
};

class targetLocation{
      public:
        double x;
        double y;
        double theta;
};
// ******************************************************************************

// Random number generator
random_numbers::RandomNumberGenerator * rng;

// Create controllers
PickUpController pickUpController;
DropOffController dropOffController;
SearchController searchController;

// Mobility Logic Functions
void sendDriveCommand(double linearVel, double angularVel);
void openFingers(); // Open fingers to 90 degrees
void closeFingers();// Close fingers to 0 degrees
void raiseWrist();  // Return wrist back to 0 degrees
void lowerWrist();  // Lower wrist to 50 degrees
void mapAverage();  // constantly averages last 100 positions from map

// ********************** ELI: Added declerations ********************
double costFunction(geometry_msgs::Pose2D currentLocation, targetLocation target);
void obstacleAvoid(int X_o, int Y_o);
void pso(cost c, personalBest pBest, globalBest glBest, geometry_msgs::Pose2D currentLocation);
double tagLocation;
double V_x;
double V_y;
double dist;
double psoFlag;
double distanceTraveled;
double w;
double rng1;
double rng2;
int sensorFlag;
int iteration;
int M[100][100];
int X_obstacle_flag;
int Y_obstacle_flag;
std::vector<int> X_obstacle;
std::vector<int> Y_obstacle;

int X;
int Y;
int non_convex_obstacle_conter;

std_msgs::Float32 map_X_msg;
std_msgs::Float32 map_Y_msg;
string non_convex_obstacle;

//********************************************************************

// Numeric Variables for rover positioning
geometry_msgs::Pose2D currentLocation;

geometry_msgs::Pose2D currentLocationAchilles;
geometry_msgs::Pose2D currentLocationAeneas;
geometry_msgs::Pose2D currentLocationAjax;

geometry_msgs::Pose2D currentLocationMap;
geometry_msgs::Pose2D currentLocationAverage;
geometry_msgs::Pose2D goalLocation;

geometry_msgs::Pose2D centerLocation;
geometry_msgs::Pose2D centerLocationMap;
geometry_msgs::Pose2D centerLocationOdom;

int currentMode = 0;
float mobilityLoopTimeStep = 0.1; // time between the mobility loop calls
float status_publish_interval = 1;
float killSwitchTimeout = 10;
bool targetDetected = false;
bool targetCollected = false;
//Eli: timer to clock when to calculate the next step in which a new goal is set
int psoTimer = 0;
int psoMoveTime = 1000;

// Set true when the target block is less than targetDist so we continue
// attempting to pick it up rather than switching to another block in view.
bool lockTarget = false;

// Failsafe state. No legitimate behavior state. If in this state for too long
// return to searching as default behavior.
bool timeOut = false;

// Set to true when the center ultrasound reads less than 0.14m. Usually means
// a picked up cube is in the way.
bool blockBlock = false;

// central collection point has been seen (aka the nest)
bool centerSeen = false;

// Set true when we are insie the center circle and we need to drop the block,
// back out, and reset the boolean cascade.
bool reachedCollectionPoint = false;

// used for calling code once but not in main
bool init = false;

// used to remember place in mapAverage array
int mapCount = 0;
int odomCount = 0;

// How many points to use in calculating the map average position
const unsigned int mapHistorySize = 500;

// An array in which to store map positions
geometry_msgs::Pose2D mapLocation[mapHistorySize];

int mapLocationFitness[mapHistorySize+3];

bool avoidingObstacle = false;

float searchVelocity = 2.2; // meters/second

std_msgs::String msg;

// state machine states
#define STATE_MACHINE_TRANSFORM 0
#define STATE_MACHINE_ROTATE 1
#define STATE_MACHINE_SKID_STEER 2
#define STATE_MACHINE_PICKUP 3
#define STATE_MACHINE_DROPOFF 4

int stateMachineState = STATE_MACHINE_TRANSFORM;

geometry_msgs::Twist velocity;
char host[128];
string publishedName;
string testType;
char prev_state_machine[128];

// Publishers
ros::Publisher stateMachinePublish;
ros::Publisher status_publisher;
ros::Publisher fingerAnglePublish;
ros::Publisher wristAnglePublish;
ros::Publisher infoLogPublisher;
ros::Publisher driveControlPublish;

//Eli: establishing a publisher to communicate globalBest
ros::Publisher globalBestPublisher;
ros::Publisher globalBest_X_Publisher;
ros::Publisher globalBest_Y_Publisher;
ros::Publisher mappingObstacle_X_Publisher;
ros::Publisher mappingObstacle_Y_Publisher;


// Subscribers
ros::Subscriber joySubscriber;
ros::Subscriber modeSubscriber;
ros::Subscriber targetSubscriber;
ros::Subscriber obstacleSubscriber;
ros::Subscriber odometrySubscriber;
//ros::Subscriber odometrySubscriberAchilles;
//ros::Subscriber odometrySubscriberAeneas;
//ros::Subscriber odometrySubscriberAjax;
ros::Subscriber mapSubscriber;


//Eli: establishing a Subscriber to receive globalBest
ros::Subscriber globalBestSubscriber;
ros::Subscriber globalBest_X_Subscriber;
ros::Subscriber globalBest_Y_Subscriber;
ros::Subscriber mappingObstacle_X_Subscriber;
ros::Subscriber mappingObstacle_Y_Subscriber;


// Timers
ros::Timer stateMachineTimer;
ros::Timer publish_status_timer;
ros::Timer targetDetectedTimer;

// ELI: declaring class objects for PSO
personalBest pBest;
globalBest glBest;
cost c;
targetLocation target;

ofstream ofPosition;
ofstream ofPbest;
ofstream ofGbest;
ofstream ofCost;
ofstream ofGcost;
ofstream ofMap;

// records time for delays in sequanced actions, 1 second resolution.
time_t timerStartTime;

// An initial delay to allow the rover to gather enough position data to
// average its location.
unsigned int startDelayInSeconds = 1;
float timerTimeElapsed = 0;

//Transforms
tf::TransformListener *tfListener;

// OS Signal Handler
void sigintEventHandler(int signal);

//Callback handlers
void joyCmdHandler(const sensor_msgs::Joy::ConstPtr& message);
void modeHandler(const std_msgs::UInt8::ConstPtr& message);
void targetHandler(const apriltags_ros::AprilTagDetectionArray::ConstPtr& tagInfo);
void obstacleHandler(const std_msgs::UInt8::ConstPtr& message);
void odometryHandler(const nav_msgs::Odometry::ConstPtr& message);
void mapHandler(const nav_msgs::Odometry::ConstPtr& message);
void mobilityStateMachine(const ros::TimerEvent&);
void publishStatusTimerEventHandler(const ros::TimerEvent& event);
void targetDetectedReset(const ros::TimerEvent& event);

void globalBestHandler(const std_msgs::Float32 message);
void globalBest_X_Handler(const std_msgs::Float32 message);
void globalBest_Y_Handler(const std_msgs::Float32 message);
void mappingObstacle_X_Handler(const std_msgs::Float32 message);
void mappingObstacle_Y_Handler(const std_msgs::Float32 message);

int main(int argc, char **argv) {

    gethostname(host, sizeof (host));
    string hostname(host);

    // instantiate random number generator
    rng = new random_numbers::RandomNumberGenerator();

    //set initial random heading
    //goalLocation.theta = 1.57;

    // Set Target destination in the x direction [m]
    target.x = 50;
    target.y = 0;
    sensorFlag = 1;

    psoFlag = 1;
    dist =  sqrt(pow(target.x - currentLocation.x, 2) + pow(target.y - currentLocation.y, 2));
    distanceTraveled = 1;

    //select initial search position 50 cm from center (0,0)
    goalLocation.x = 0;
    goalLocation.y = 0;

    goalLocation.theta = atan2(target.y, target.x); //initializing rovers to head in target's direction

    centerLocation.x = 0;
    centerLocation.y = 0;
    centerLocationOdom.x = 0;
    centerLocationOdom.y = 0;

    pBest.cost = 300;                       // Extrem cost best value to be reset first pass
    pBest.x  = currentLocation.x;           // initializing x best
    pBest.y  = currentLocation.y;           // initializing y best
    pBest.theta  = currentLocation.theta;   // initializing theta best
    iteration = -1;

    mapLocationFitness[0] = 1;
    mapLocationFitness[1] = 1;
    mapLocationFitness[2] = 1;

    // ELi: initializing global bests
    glBest.x  = pBest.x;
    glBest.y  = pBest.y;
    glBest.theta  = pBest.theta;
    glBest.cost = pBest.cost;

    V_x = 0;
    V_y = 0;
    w = 0.9;


    for (int i=0; i<100; i++)
        for (int j=0; j<100; j++) (M[i][j] = 0);


    for (int i = 0; i < 100; i++) {
        mapLocation[i].x = 0;
        mapLocation[i].y = 0;
        mapLocation[i].theta = 0;
    }

    if (argc >= 2) {
        publishedName = argv[1];
        cout << "Welcome to the world of tomorrow " << publishedName
             << "!  Mobility turnDirectionule started." << endl;
    } else {
        publishedName = hostname;
        cout << "No Name Selected. Default is: " << publishedName << endl;
    }

    // NoSignalHandler so we can catch SIGINT ourselves and shutdown the node
    ros::init(argc, argv, (publishedName + "_MOBILITY"), ros::init_options::NoSigintHandler);
    ros::NodeHandle mNH;

    // Register the SIGINT event handler so the node can shutdown properly
    signal(SIGINT, sigintEventHandler);

    joySubscriber = mNH.subscribe((publishedName + "/joystick"), 10, joyCmdHandler);
    modeSubscriber = mNH.subscribe((publishedName + "/mode"), 1, modeHandler);
    targetSubscriber = mNH.subscribe((publishedName + "/targets"), 10, targetHandler);
    obstacleSubscriber = mNH.subscribe((publishedName + "/obstacle"), 10, obstacleHandler);
    odometrySubscriber = mNH.subscribe((publishedName + "/odom/filtered"), 10, odometryHandler);
    mapSubscriber = mNH.subscribe((publishedName + "/odom/ekf"), 10, mapHandler);

    globalBestSubscriber = mNH.subscribe(("/gbestCost"), 10, globalBestHandler);
    globalBest_X_Subscriber = mNH.subscribe(("/gbest_X"), 10, globalBest_X_Handler);
    globalBest_Y_Subscriber = mNH.subscribe(("/gbest_y"), 10, globalBest_Y_Handler);
    mappingObstacle_X_Subscriber = mNH.subscribe(("/mappingObstacle_X"), 10, mappingObstacle_X_Handler);
    mappingObstacle_Y_Subscriber = mNH.subscribe(("/mappingObstacle_Y"), 10, mappingObstacle_Y_Handler);

    globalBestPublisher = mNH.advertise<std_msgs::Float32>(("/gbestCost"), 10, true);
    globalBest_X_Publisher = mNH.advertise<std_msgs::Float32>(("/gbest_X"), 10, true);
    globalBest_Y_Publisher = mNH.advertise<std_msgs::Float32>(("/gbest_y"), 10, true);
    mappingObstacle_X_Publisher = mNH.advertise<std_msgs::Float32>(("/mappingObstacle_X"), 10, true);
    mappingObstacle_Y_Publisher = mNH.advertise<std_msgs::Float32>(("/mappingObstacle_Y"), 10, true);

    status_publisher = mNH.advertise<std_msgs::String>((publishedName + "/status"), 1, true);
    stateMachinePublish = mNH.advertise<std_msgs::String>((publishedName + "/state_machine"), 1, true);
    fingerAnglePublish = mNH.advertise<std_msgs::Float32>((publishedName + "/fingerAngle/cmd"), 1, true);
    wristAnglePublish = mNH.advertise<std_msgs::Float32>((publishedName + "/wristAngle/cmd"), 1, true);
    infoLogPublisher = mNH.advertise<std_msgs::String>("/infoLog", 10, true);
    driveControlPublish = mNH.advertise<geometry_msgs::Twist>((publishedName + "/driveControl"), 10);

    publish_status_timer = mNH.createTimer(ros::Duration(status_publish_interval), publishStatusTimerEventHandler);
    stateMachineTimer = mNH.createTimer(ros::Duration(mobilityLoopTimeStep), mobilityStateMachine);
    targetDetectedTimer = mNH.createTimer(ros::Duration(0), targetDetectedReset, true);

    tfListener = new tf::TransformListener();
    std_msgs::String msg;
    msg.data = "Log Started";
    infoLogPublisher.publish(msg);

    stringstream ss;
    ss << "Rover start delay set to " << startDelayInSeconds << " seconds";
    msg.data = ss.str();
    infoLogPublisher.publish(msg);

    timerStartTime = time(0);

    ros::spin();

    return EXIT_SUCCESS;
}


// This is the top-most logic control block organised as a state machine.
// This function calls the dropOff, pickUp, and search controllers.
// This block passes the goal location to the proportional-integral-derivative
// controllers in the abridge package.
void mobilityStateMachine(const ros::TimerEvent&)
{
    std_msgs::String stateMachineMsg;
    float rotateOnlyAngleTolerance = 0.2;
    int returnToSearchDelay = 5;

    // calls the averaging function, also responsible for
    // transform from Map frame to odom frame.
    mapAverage();

    // Robot is in automode
    if (currentMode == 2 || currentMode == 3)
    {
        // time since timerStartTime was set to current time
        timerTimeElapsed = time(0) - timerStartTime;
        iteration = iteration + 1;
        ////ROS_INFO("goalLocation = %lf, %lf", goalLocation.x, goalLocation.y);

        ofPosition << iteration << " " << currentLocation.x << " " << currentLocation.y << "\n";

        // Waits till both X and Y obstacle location are published
        // stores both values at the end of each vector at the same index
        if(X_obstacle_flag != 0 && Y_obstacle_flag != 0)
        {
          if(X_obstacle_flag != 99 && Y_obstacle_flag != 99)
          {
            //ROS_INFO("line 443: Tagging published obstacle location %d, %d", X_obstacle_flag, Y_obstacle_flag);

            X_obstacle.push_back(X_obstacle_flag);
            Y_obstacle.push_back(Y_obstacle_flag);

            X_obstacle_flag = 99;
            Y_obstacle_flag = 99;
          }
        }

        if(currentLocation.x < goalLocation.x + 0.25 && currentLocation.x > goalLocation.x - 0.25)
        {
          if (currentLocation.y < goalLocation.y + 0.25 && currentLocation.y > goalLocation.y - 0.25)
          {
            ////ROS_INFO("******* Reached goal location **********");
            psoFlag = 1;

          }
        }

        if(currentLocation.x < target.x + 0.25 && currentLocation.x > target.x - 0.25)
        {
            if (currentLocation.y < target.y + 0.25 && currentLocation.y > target.y - 0.25)
            {
              //ROS_INFO("******* Reached target **********");
              sendDriveCommand(0,0);
              psoFlag = 0;
              sensorFlag = 0;
            }
        }

        // init code goes here. (code that runs only once at start of
        // auto mode but wont work in main goes here)
        if (!init)
        {
            if (timerTimeElapsed > startDelayInSeconds)
            {
                // Set the location of the center circle location in the map
                // frame based upon our current average location on the map.
                centerLocationMap.x = currentLocationAverage.x;
                centerLocationMap.y = currentLocationAverage.y;
                centerLocationMap.theta = currentLocationAverage.theta;

                string testInfo;


                //testInfo = "\nNEW RUN, Test Info \n# of Rovers: 6 \n# of obstacles: 2 \nObstacle 1 Location: (5, 1) \nObstacle 2 Location: (5, -1) \nTarget Location(x,y): (10,0) \n";
                //testInfo = "\nNEW RUN, Test Info \n# of Rovers: 6 \n# of obstacles: 1 \nObstacle 1 Location: (5, 0) \nTarget Location(x,y): (10,0) \n";
                //testInfo = "\nNEW RUN, Test Info \n# of Rovers: 4 \n# of obstacles: 0 \nTarget Location(x,y): (50,0) \n";
                //ofPosition.open("testInformation.txt", std::ofstream::out | std::ofstream::trunc);
                //ofPosition << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";
                //ofPosition.close();

                if (publishedName == "achilles")
                {
                   ofPosition.open("2_rover2/achillesPosition.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPosition << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";

                   ofPbest.open("2_rover2/achillesPbest.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPbest << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";
                }
                else if (publishedName == "aeneas")
                {
                   ofPosition.open("2_rover2/aeneasPosition.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPosition << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";

                   ofPbest.open("2_rover2/aeneasPbest.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPbest << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";
                }
                else if (publishedName == "ajax")
                {
                   ofPosition.open("2_rover2/ajaxPosition.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPosition << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";

                   ofPbest.open("2_rover2/ajaxPbest.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPbest << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";
                }
                else if (publishedName == "diomedes")
                {
                   ofPosition.open("2_rover2/diomedesPosition.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPosition << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";

                   ofPbest.open("2_rover2/diomedesPbest.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPbest << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";
                }
                else if (publishedName == "hector")
                {
                   ofPosition.open("2_rover2/hectorPosition.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPosition << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";

                   ofPbest.open("2_rover2/hectorPbest.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPbest << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";
                }
                else if (publishedName == "paris")
                {
                   ofPosition.open("2_rover2/parisPosition.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPosition << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";

                   ofPbest.open("2_rover2/parisPbest.txt", std::ofstream::out | std::ofstream::trunc);
                   //ofPbest << "\n\n\n\n" << "***************************" << testInfo << "***************************" << "\n\n\n\n";
                }

                ofGbest.open("2_rover2/globalBestPosition.txt", std::ofstream::out | std::ofstream::trunc);
                ofGcost.open("2_rover2/globalBestCost.txt", std::ofstream::out | std::ofstream::trunc);

                //ofMap.open("map.txt", std::ofstream::out | std::ofstream::trunc);



                // Eli initializing global best cost to worst value
                glBest.costMsg.data = glBest.cost;
                glBest.X_Msg.data = glBest.x;
                glBest.Y_Msg.data = glBest.y;
                globalBestPublisher.publish(glBest.costMsg);
                globalBest_X_Publisher.publish(glBest.X_Msg);
                globalBest_Y_Publisher.publish(glBest.Y_Msg);


                // initialization has run
                init = true;
            }
            else
            {
              return;
            }
        }

        // If no collected or detected blocks set fingers
        // to open wide and raised position.
        if (!targetCollected && !targetDetected)
        {
            // set gripper
            std_msgs::Float32 angle;

            // open fingers
            angle.data = M_PI_2;

            fingerAnglePublish.publish(angle);
            angle.data = 0;

            // raise wrist
            wristAnglePublish.publish(angle);
        }

        // Select rotation or translation based on required adjustment
        switch(stateMachineState)
        {
            //ROS_INFO("____TUNNING PSO____");
            pso(c, pBest, glBest, currentLocation);

          // If no adjustment needed, select new goal
          case STATE_MACHINE_TRANSFORM:
          {
              stateMachineMsg.data = "TRANSFORMING";

              //******************* If angle between current and goal is significant *******************
              //******************* if error in heading is greater than 0.4 radians *******************
              if (fabs(angles::shortest_angular_distance(currentLocation.theta, goalLocation.theta)) > rotateOnlyAngleTolerance)
              {
                  stateMachineState = STATE_MACHINE_ROTATE;
              }


              //******************* If goal has not yet been reached drive and maintane heading *******************
              else if (fabs(angles::shortest_angular_distance(currentLocation.theta, atan2(goalLocation.y - currentLocation.y, goalLocation.x - currentLocation.x))) < M_PI_2)
              {
                  stateMachineState = STATE_MACHINE_SKID_STEER;
              }


              //******************* Otherwise, drop off target and select new random uniform heading ******************
              //*******************  If no targets have been detected, assign a new goal *******************
              else if (!targetDetected && timerTimeElapsed > returnToSearchDelay)
              {
                  pso(c, pBest, glBest, currentLocation);
              }

              //Purposefully fall through to next case without breaking
          }



          // Calculate angle between currentLocation.theta and goalLocation.theta
          // Rotate left or right depending on sign of angle
          // Stay in this state until angle is minimized
          case STATE_MACHINE_ROTATE:
          {
              stateMachineMsg.data = "ROTATING";
              // Calculate the diffrence between current and desired
              // heading in radians.
              float errorYaw = angles::shortest_angular_distance(currentLocation.theta, goalLocation.theta);

              // If angle > 0.4 radians rotate but dont drive forward.
              if (fabs(angles::shortest_angular_distance(currentLocation.theta, goalLocation.theta)) > rotateOnlyAngleTolerance) {
                  // rotate but dont drive  0.05 is to prevent turning in reverse
                  sendDriveCommand(0.05, errorYaw);
                  break;
              } else {
                  // move to differential drive step
                  stateMachineState = STATE_MACHINE_SKID_STEER;
                  //fall through on purpose.
              }
          }



          // Calculate angle between currentLocation.x/y and goalLocation.x/y
          // Drive forward
          // Stay in this state until angle is at least PI/2
          case STATE_MACHINE_SKID_STEER:
          {
              stateMachineMsg.data = "SKID_STEER";

              // calculate the distance between current and desired heading in radians
              float errorYaw = angles::shortest_angular_distance(currentLocation.theta, goalLocation.theta);

              // goal not yet reached drive while maintaining proper heading.
              if (fabs(angles::shortest_angular_distance(currentLocation.theta, atan2(goalLocation.y - currentLocation.y, goalLocation.x - currentLocation.x))) < M_PI_2) {
                  // drive and turn simultaniously
                  sendDriveCommand(searchVelocity, errorYaw/2);
              }
              // goal is reached but desired heading is still wrong turn only
              else if (fabs(angles::shortest_angular_distance(currentLocation.theta, goalLocation.theta)) > 0.1) {
                   // rotate but dont drive
                  sendDriveCommand(0.0, errorYaw);
              }
              else {
                  // stop
                  sendDriveCommand(0.0, 0.0);
                  avoidingObstacle = false;
                  psoFlag = 1;

                  // move back to transform step
                  stateMachineState = STATE_MACHINE_TRANSFORM;
              }

              break;
          }



          case STATE_MACHINE_PICKUP:
          {
              stateMachineMsg.data = "PICKUP";

              PickUpResult result;

              // we see a block and have not picked one up yet
              if (targetDetected && !targetCollected) {
                  result = pickUpController.pickUpSelectedTarget(blockBlock);
                  sendDriveCommand(result.cmdVel,result.angleError);
                  std_msgs::Float32 angle;

                  if (result.fingerAngle != -1) {
                      angle.data = result.fingerAngle;
                      fingerAnglePublish.publish(angle);
                  }

                  if (result.wristAngle != -1) {
                      angle.data = result.wristAngle;

                      // raise wrist
                      wristAnglePublish.publish(angle);
                  }

                  if (result.giveUp) {
                      targetDetected = false;
                      stateMachineState = STATE_MACHINE_TRANSFORM;
                      sendDriveCommand(0,0);
                      pickUpController.reset();
                  }

                  if (result.pickedUp) {
                      pickUpController.reset();

                      // assume target has been picked up by gripper
                      targetCollected = true;
                      result.pickedUp = false;
                      stateMachineState = STATE_MACHINE_ROTATE;

                      goalLocation.theta = atan2(centerLocationOdom.y - currentLocation.y, centerLocationOdom.x - currentLocation.x);

                      // set center as goal position
                      goalLocation.x = centerLocationOdom.x = 0;
                      goalLocation.y = centerLocationOdom.y;

                      // lower wrist to avoid ultrasound sensors
                      std_msgs::Float32 angle;
                      angle.data = 0.8;
                      wristAnglePublish.publish(angle);
                      sendDriveCommand(0.0,0);

                      return;
                  }
              }
              else
              {
                  stateMachineState = STATE_MACHINE_TRANSFORM;
              }
              break;
          }

          case STATE_MACHINE_DROPOFF:
          {
              stateMachineMsg.data = "DROPOFF";
              break;
          }

          default:
          {
              break;
          }
      } /* end of switch() */
    }
    // mode is NOT auto
    else {
        // publish current state for the operator to see
        stateMachineMsg.data = "WAITING";
        ofPosition.close();
        ofPbest.close();
        ofGbest.close();
        ofMap.close();
    }

    // publish state machine string for user, only if it has changed, though
    if (strcmp(stateMachineMsg.data.c_str(), prev_state_machine) != 0) {
        stateMachinePublish.publish(stateMachineMsg);
        sprintf(prev_state_machine, "%s", stateMachineMsg.data.c_str());
    }
}









/*************************
 * ROS CALLBACK HANDLERS *
 *************************/

void sendDriveCommand(double linearVel, double angularError)
{
    velocity.linear.x = linearVel,
    velocity.angular.z = angularError;

    // publish the drive commands
    driveControlPublish.publish(velocity);
}



void targetHandler(const apriltags_ros::AprilTagDetectionArray::ConstPtr& message) {

    // If in manual mode do not try to automatically pick up the target
    if (currentMode == 1 || currentMode == 0) return;

        /***************************** ELI: test ***************************
    // if a target is detected and we are looking for center tags
    if (message->detections.size() > 0 && !reachedCollectionPoint) {
        float cameraOffsetCorrection = 0.020; //meters;

        centerSeen = false;
        double count = 0;
        double countRight = 0;
        double countLeft = 0;

        // this loop is to get the number of center tags

        for (int i = 0; i < message->detections.size(); i++) {
            if (message->detections[i].id == 256) {
                geometry_msgs::PoseStamped cenPose = message->detections[i].pose;

                // checks if tag is on the right or left side of the image
                if (cenPose.pose.position.x + cameraOffsetCorrection > 0) {
                    countRight++;

                } else {
                    countLeft++;
                }

                centerSeen = true;
                count++;
            }
        }

        if (centerSeen && targetCollected) {
            stateMachineState = STATE_MACHINE_TRANSFORM;
            goalLocation = currentLocation;
        }

        dropOffController.setDataTargets(count,countLeft,countRight);

        // if we see the center and we dont have a target collected

        if (centerSeen && !targetCollected) {

            float centeringTurn = 0.15; //radians
            stateMachineState = STATE_MACHINE_TRANSFORM;

            // this code keeps the robot from driving over
            // the center when searching for blocks
            if (right) {
                // turn away from the center to the left if just driving
                // around/searching.
                goalLocation.theta += centeringTurn;
            } else {
                // turn away from the center to the right if just driving
                // around/searching.
                goalLocation.theta -= centeringTurn;
            }

            // continues an interrupted search
            goalLocation = searchController.continueInterruptedSearch(currentLocation, goalLocation);

            targetDetected = false;
            pickUpController.reset();

            return;

    }
    // end found target and looking for center tags

    // found a target april tag and looking for april cubes;
    // with safety timer at greater than 5 seconds.
    PickUpResult result;

    if (message->detections.size() > 0 && !targetCollected && timerTimeElapsed > 5) {
        targetDetected = true;

        // pickup state so target handler can take over driving.
        stateMachineState = STATE_MACHINE_PICKUP;
        result = pickUpController.selectTarget(message);

        std_msgs::Float32 angle;

        if (result.fingerAngle != -1) {
            angle.data = result.fingerAngle;
            fingerAnglePublish.publish(angle);
        }

        if (result.wristAngle != -1) {
            angle.data = result.wristAngle;
            wristAnglePublish.publish(angle);
        }
    }
  } ***************************************************************************/
}

void modeHandler(const std_msgs::UInt8::ConstPtr& message)
{
    currentMode = message->data;
    sendDriveCommand(0.0, 0.0);
}

void obstacleHandler(const std_msgs::UInt8::ConstPtr& message) // Obstacle Handler portion that records obstacle encounters
{
    if ((!targetDetected || targetCollected) && (message->data > 0))
    {
          // Rounding down the the coordinate at when an obstacle is encountered
          X = std::floor(currentLocation.x);
          Y = std::floor(currentLocation.y);

          map_X_msg.data = X;
          map_Y_msg.data = Y;

          // Logging the location of all the obstacles encountered with a check to not log same position twice if within plus and minus 1 meter zone
          int vectorSize = X_obstacle.size();
          string isObstacleTagged = "NO";
          for (int i = 0; i < vectorSize; i++)
          {
            if((currentLocation.x < X_obstacle[i] + 1 && currentLocation.x > X_obstacle[i] - 1) && (currentLocation.y < Y_obstacle[i] + 1 && currentLocation.y > Y_obstacle[i] - 1))
            {
              if (i > 5)
              {
                if(abs(currentLocation.x - X_obstacle[i]) < 0.5)
                {
                  non_convex_obstacle_conter = non_convex_obstacle_conter + 1;
                  if (non_convex_obstacle_conter == 5)
                  {
                    non_convex_obstacle = "true";
                  }
                }
              }
              isObstacleTagged = "Yes";
            }
          }
          if(isObstacleTagged == "NO")
          {
            // If obstacle is not tagged, the coordinate is published to other swarm members
            mappingObstacle_X_Publisher.publish(map_X_msg);
            mappingObstacle_Y_Publisher.publish(map_Y_msg);
          }

        if (message->data == 1)
        {
          // select new heading 0.2 radians to the left
          goalLocation.theta = currentLocation.theta + 0.5;
        }

        // obstacle in front or on left side
        else if (message->data == 2)
        {
          // select new heading 0.2 radians to the right
          goalLocation.theta = currentLocation.theta - 0.5;
        }

        // continues an interrupted search
        goalLocation = searchController.continueInterruptedSearch(currentLocation, goalLocation);
        goalLocation.theta = atan2(goalLocation.y - currentLocation.y, goalLocation.x - currentLocation.x);

        // switch to transform state to trigger collision avoidance
        stateMachineState = STATE_MACHINE_ROTATE;

        avoidingObstacle = true;
  }

  // the front ultrasond is blocked very closely. 0.14m currently
  if (message->data == 4)
  {
      blockBlock = true;
  }
  else
  {
      blockBlock = false;
  }
}

void odometryHandler(const nav_msgs::Odometry::ConstPtr& message)
{
    //Get (x,y) location directly from pose
    currentLocation.x = message->pose.pose.position.x;
    currentLocation.y = message->pose.pose.position.y;

    //Get theta rotation by converting quaternion orientation to pitch/roll/yaw
    tf::Quaternion q(message->pose.pose.orientation.x, message->pose.pose.orientation.y, message->pose.pose.orientation.z, message->pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    currentLocation.theta = yaw;
}

void mapHandler(const nav_msgs::Odometry::ConstPtr& message)
{
    //Get (x,y) location directly from pose
    currentLocationMap.x = message->pose.pose.position.x;
    currentLocationMap.y = message->pose.pose.position.y;

    //Get theta rotation by converting quaternion orientation to pitch/roll/yaw
    tf::Quaternion q(message->pose.pose.orientation.x, message->pose.pose.orientation.y, message->pose.pose.orientation.z, message->pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    currentLocationMap.theta = yaw;
}

void joyCmdHandler(const sensor_msgs::Joy::ConstPtr& message)
{
    if (currentMode == 0 || currentMode == 1) {
        sendDriveCommand(abs(message->axes[4]) >= 0.1 ? message->axes[4] : 0, abs(message->axes[3]) >= 0.1 ? message->axes[3] : 0);
    }
}


void publishStatusTimerEventHandler(const ros::TimerEvent&)
{
    std_msgs::String msg;
    msg.data = "online";
    status_publisher.publish(msg);
}


void targetDetectedReset(const ros::TimerEvent& event)
{
    /*
    targetDetected = false;

    std_msgs::Float32 angle;
    angle.data = 0;

    // close fingers
    fingerAnglePublish.publish(angle);

    // raise wrist
    wristAnglePublish.publish(angle);
    */
}

void sigintEventHandler(int sig)
{
    // All the default sigint handler does is call shutdown()
    ros::shutdown();
}

void mapAverage()
{
    // store currentLocation in the averaging array
    mapLocation[mapCount] = currentLocationMap;
    mapLocationFitness[mapCount+2] = 1;
    mapCount++;

    if (mapCount >= mapHistorySize) {
        mapCount = 0;
    }

    double x = 0;
    double y = 0;
    double theta = 0;

    // add up all the positions in the array
    for (int i = 0; i < mapHistorySize; i++) {
        x += mapLocation[i].x;
        y += mapLocation[i].y;
        theta += mapLocation[i].theta;
    }

    // find the average
    x = x/mapHistorySize;
    y = y/mapHistorySize;

    // Get theta rotation by converting quaternion orientation to pitch/roll/yaw
    theta = theta/100;
    currentLocationAverage.x = x;
    currentLocationAverage.y = y;
    currentLocationAverage.theta = theta;


    // only run below code if a centerLocation has been set by initilization
    if (init)
    {
        // map frame
        geometry_msgs::PoseStamped mapPose;

        // setup msg to represent the center location in map frame
        mapPose.header.stamp = ros::Time::now();

        mapPose.header.frame_id = publishedName + "/map";
        mapPose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0, 0, centerLocationMap.theta);
        mapPose.pose.position.x = centerLocationMap.x;
        mapPose.pose.position.y = centerLocationMap.y;
        geometry_msgs::PoseStamped odomPose;
        string x = "";

        try { //attempt to get the transform of the center point in map frame to odom frame.
            tfListener->waitForTransform(publishedName + "/map", publishedName + "/odom", ros::Time::now(), ros::Duration(1.0));
            tfListener->transformPose(publishedName + "/odom", mapPose, odomPose);
        }

        catch(tf::TransformException& ex) {
            //ROS_INFO("Received an exception trying to transform a point from \"map\" to \"odom\": %s", ex.what());
            x = "Exception thrown " + (string)ex.what();
            std_msgs::String msg;
            stringstream ss;
            ss << "Exception in mapAverage() " + (string)ex.what();
            msg.data = ss.str();
            infoLogPublisher.publish(msg);
        }

        // Use the position and orientation provided by the ros transform.
        centerLocation.x = odomPose.pose.position.x; //set centerLocation in odom frame
        centerLocation.y = odomPose.pose.position.y;


    }
}

void globalBestHandler(const std_msgs::Float32 globalBestCost)
{
  if (globalBestCost.data < glBest.cost)
  {
      glBest.cost = globalBestCost.data;
  }
}

void globalBest_X_Handler(const std_msgs::Float32 globalBest_X)
{
  glBest.x = globalBest_X.data;
}

void globalBest_Y_Handler(const std_msgs::Float32 globalBest_Y)
{
  glBest.y = globalBest_Y.data;
}

void mappingObstacle_X_Handler(const std_msgs::Float32 X_obstacle_flag_msg)
{
  X_obstacle_flag = X_obstacle_flag_msg.data;
}

void mappingObstacle_Y_Handler(const std_msgs::Float32 Y_obstacle_flag_msg)
{
  Y_obstacle_flag = Y_obstacle_flag_msg.data;
}

double costFunction(geometry_msgs::Pose2D currentLocation, targetLocation target)
{
  double fitnessValue;
  fitnessValue = sqrt(pow(target.x - currentLocation.x, 2.0) + pow((target.y - currentLocation.y ), 2.0));

  return fitnessValue;
}

void obstacleAvoid(int X_o, int Y_o) // Obstacle Handler
{
  double theta_A;
  double theta_B;
  double X_A; double Y_A;
  double X_B; double Y_B;

  double d_X_A; double d_Y_A;
  double d_X_B; double d_Y_B;
  int d;

  // (d) should be set to a larger value then the buffer zone.
  // It is the distance to travle when attempting to circumvent an obstacle
  d=3;

  /*********************************************************************************************
  ***Using "*_A" committe rovers to turning to their left keeping the obstacle to their right***
  ***Using "*_B" committe rovers to turning to their right keeping the obstacle to their left***
  ***only one, either "*_A" or "*_B" can be used. The other must be commented out***************
  *********************************************************************************************/

  //theta_A = currentLocation.theta - 1.57;
  theta_B = currentLocation.theta + 1.57;

  //d_X_A = d * cos(theta_A);   d_Y_A = d * sin(theta_A);
  d_X_B = d * cos(theta_B);   d_Y_B = d * sin(theta_B);

  //X_A = X_o + d_X_A;          Y_A = Y_o + d_Y_A;
  X_B = X_o + d_X_B;          Y_B = Y_o + d_Y_B;


  double angleToObstacle = atan2((Y_B - currentLocation.y), (X_B - currentLocation.x));
  double angleToTarget= atan2((target.y - currentLocation.y), (target.x - currentLocation.x));

  float targetObstacleAngleDiff = abs(angleToObstacle - angleToTarget);


  // Checking for the 180 condition plus and minus a tolerance of 8 degrees
  if (targetObstacleAngleDiff < 3.0)
  {
    goalLocation.x = X_B;   goalLocation.y = Y_B;
    goalLocation.theta = atan2((goalLocation.y - currentLocation.y), (goalLocation.x - currentLocation.x));
  }
  else{
    ROS_INFO("Not Avoiding Obstacle, Moving to Target");
    non_convex_obstacle = "false";
    non_convex_obstacle_conter = 0;
  }
}


void pso(cost c, personalBest pBest, globalBest glBest, geometry_msgs::Pose2D currentLocation)
{

  // Evaluate Cost
  c.rover = costFunction(currentLocation, target);

  // Check for Personal Best & Cost
  if (c.rover < pBest.cost)
  {
    pBest.x = currentLocation.x;
    pBest.y = currentLocation.y;
    pBest.cost  = c.rover;

    ofPbest << iteration << " " << pBest.x << " " << pBest.y << "\n" ;

    if (c.rover < glBest.cost)
    {
      glBest.x  = pBest.x;
      glBest.y  = pBest.y;
      //glBest.theta  = pBest.theta;
      glBest.cost = c.rover;

      ofGcost << iteration << " " << glBest.cost << "\n";
      ofGbest << iteration << " " << glBest.x << " " << glBest.y << "\n";

      // publishing global best cost

      glBest.costMsg.data = glBest.cost;
      glBest.X_Msg.data = glBest.x;
      glBest.Y_Msg.data = glBest.y;
      ////ROS_INFO("glBest._Msg.data x, y::: (%lf, %lf)", glBest.X_Msg.data, glBest.Y_Msg.data);
      globalBestPublisher.publish(glBest.costMsg);
      globalBest_X_Publisher.publish(glBest.X_Msg);
      globalBest_Y_Publisher.publish(glBest.Y_Msg);
    }
  }
  if (psoFlag == 1)
  {

      rng1 = ((double) rand()/(RAND_MAX));     // Cognitive randome number coefficient
      rng2 = ((double) rand()/(RAND_MAX));     // Social randome number Coefficient
      int c1 = 2;                              // Cognitive acceleration coefficient
      int c2 = 2;                              // Social acceleration coefficient
      int bufferZone = 2;                      // A obstacle zone about an obstacle marked coordinate in meters
      double V_max = 0.2 * target.x;           // 20 perecent of the Target Distance away from home base
      int V_min = -V_max;
      double distToGoal = sqrt(pow(target.x, 2) + pow(target.y, 2));
      double distToRover = sqrt(pow(currentLocation.x, 2) + pow(currentLocation.y, 2));
      double distToTarget = sqrt(pow((currentLocation.x - target.x), 2) + pow((currentLocation.y - target.y), 2));

      w = abs(1 - (distToRover/distToGoal));


      // Limiting The Momentum terms values to %20 of the distance to the target in both x and y direction
      V_x =  w * V_x + c1 * rng1 * (pBest.x - currentLocation.x) + c2 * rng2 * (glBest.x - currentLocation.x);
      if(V_x < V_min)
      {
        V_x = V_min;
      }
      else if (V_x  > V_max)
      {
        V_x = V_max;
      }

      V_y =  w * V_y + c1 * rng1 * (pBest.y - currentLocation.y) + c2 * rng2 * (glBest.y - currentLocation.y);
      if(V_y < V_min)
      {
        V_y = V_min;
      }
      else if (V_y  > V_max)
      {
        V_y = V_max;
      }

      // Updating the goal which is the intermediate step to the target
      goalLocation.x = (currentLocation.x) + V_x;
      goalLocation.y = (currentLocation.y) + V_y;
      goalLocation.theta = atan2((goalLocation.y - currentLocation.y), (goalLocation.x - currentLocation.x));

      psoFlag = 0;
      /*
      if (distToTarget > 5) //Ignoring any encounters (Obstacle Hanlder) when in range of target (meters)
      {
          // Looping through obstacle coordinates to see if current location is wihtin a buffer zone
          int vectorSize = X_obstacle.size();
          for (int i = vectorSize; i > 0; i--)
          {
            if((currentLocation.x < X_obstacle[i] + bufferZone && currentLocation.x > X_obstacle[i] - bufferZone) && (currentLocation.y < Y_obstacle[i] + bufferZone && currentLocation.y > Y_obstacle[i] - bufferZone))
            {
              if (X_obstacle[i] != 0 && Y_obstacle[i] != 0)
              {
                ROS_INFO("Moving to avoid obstacle: %d, %d", X_obstacle[i], Y_obstacle[i]);
                obstacleAvoid(X_obstacle[i], Y_obstacle[i]); // Invoke obstacle handler
                break;
              }
            }
          }
      } */
  }
}
